const User = require('../models/user');

module.exports = {
    userSignUp,
    getUserInfo
};

function userSignUp(req, res, next) {
    let { username, password, firstname, middlename, lastname, birthday  } = req.body;
    if (!username) return res.json(new Error('Нет обязательных полей!'));
    User.findOne({ userlogin: username }, (err, user) => {
        if(err) return res.json(new Error(err));
        if (user) {
            res.json(new Error('Такой пользователь уже существует!'));
        } else {
            let newUser = new User();
            newUser.userlogin = username;
            newUser.password = password;
            newUser.firstName = firstname;
            newUser.middleName = middlename;
            newUser.lastName = lastname;
            newUser.birthdate = birthday;
            newUser.save((err, doc) => {
                if (err) res.json(new Error(err));
                return res.json(doc);
            });
        }
    });
}

function getUserInfo(req, res, next) {
    let { userID } = req.body;
    if (!userID) return res.json(new Error('Нет ID'));
    User.findById(userID, (err, user) => {
        if (err) return res.json(new Error(err));
        res.json(user);
    });
}