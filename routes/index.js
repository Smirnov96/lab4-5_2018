var express = require('express');
var router = express.Router();
const { userSignUp, getUserInfo } = require('../workers/user_workers');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/sign-up',userSignUp);
router.post('/get-user-info', getUserInfo);

module.exports = router;
